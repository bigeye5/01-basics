import axios from 'axios';

const ROOT_URL = 'http://localhost:8090/api';

export default {
  findAllPersons() {
    // eslint-disable-next-line
    console.log('getting persons');
    return axios.get(`${ROOT_URL}/person`);
  }
};
