import services from '../../api/services';

const state = {
  persons: []
};

const getters = {
  allPersons: state => state.persons
};

const actions = {
  async findAllPersons({ commit }) {
    const response = await services.findAllPersons();
    commit('setPersons', response.data);
  }
};

const mutations = {
  setPersons: (state, persons) => {
    state.persons = persons;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
