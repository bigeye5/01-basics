import Vuex from 'vuex';
import Vue from 'vue';
import person from './modules/person';

// allows Vuex to talk to Vue
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    person
  }
});
