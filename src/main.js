import Vue from 'vue';
import App from './App.vue';
import store from './store';

export const eventBus = new Vue({
  methods: {
    changeAge(age) {
      this.$emit('ageWasEdited', age);
    },
    changeName(name) {
      this.$emit('nameWasEdited', name);
    }
  }
});

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
